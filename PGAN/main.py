from __future__ import print_function
import argparse
from datetime import datetime
import os
import sys
import time

from PIL import Image

import tensorflow as tf
import numpy as np

from deeplab_resnet import DeepLabResNetModel, ImageReader, decode_labels, prepare_label

import pdb

from utils.filters import *

IMG_MEAN = np.array((104.00698793,116.66876762,122.67891434), dtype=np.float32)
    
CURRENT_DIR = os.getcwd()
NUM_CLASSES = 7
DATA_LIST = r'.\dataset\dance.txt'
DATA_LIST= os.path.join(CURRENT_DIR, 'dataset', 'dance.txt')
OUTPUT_DIR = r'.\output'
OUTPUT_DIR = os.path.join(CURRENT_DIR, 'output')
#MODEL_WEIGHTS = os.path.join(CURRENT_DIR,'models', 'checkpoint','final_model')
MODEL_WEIGHTS= r'.\models\final_model\model.ckpt-19315'
IMAGE_PATH=r'.\input'
IMAGE_PATH = os.path.join(CURRENT_DIR, 'input')
def get_arguments():
    """Parse all the arguments provided from the CLI.
    
    Returns:
      A list of parsed arguments.
    """
    parser = argparse.ArgumentParser(description="DeepLabLFOV Network Inference.")
    parser.add_argument('--input', type=str, help = 'Input image path', default = os.path.join(IMAGE_PATH,'1.jpg') )
    parser.add_argument("--output", type=str, default=OUTPUT_DIR,
                    help="Where to save predicted mask.")
    parser.add_argument("--model_weights", type=str,
                        help="Path to the file with model weights.", default=MODEL_WEIGHTS)
    parser.add_argument("--num-classes", type=int, default=NUM_CLASSES,
                        help="Number of classes to predict (including background).")

    return parser.parse_args()

def load(saver, sess, ckpt_path):
    '''Load trained weights.
    
    Args:
      saver: TensorFlow saver object.
      sess: TensorFlow session.
      ckpt_path: path to checkpoint file with parameters.
    ''' 
    saver.restore(sess, ckpt_path)

class PGAN():
    def __init__(self):
        pass
    def run(self):
        args = get_arguments()
        #num_steps = file_len(args.data_list)
        num_steps=1
        # Create queue coordinator.
        coord = tf.train.Coordinator()
        
        # Load reader.
        frame = Image.open('./input/1.jpg')
        image_batch = tf.placeholder(tf.float32, shape=[None,frame.size[1],frame.size[0],3]) # Add one batch dimension.
        
        # Create network.
        net = DeepLabResNetModel({'data': image_batch}, is_training=False, num_classes=args.num_classes)

        # Which variables to load.
        restore_var = tf.global_variables()

        # Predictions.
        raw_output = net.layers['fc1_voc12']
        raw_output_up = tf.image.resize_bilinear(raw_output, tf.shape(image_batch)[1:3,])
        raw_output_up = tf.argmax(raw_output_up, dimension=3)
        pred = tf.expand_dims(raw_output_up, dim=3)

        
        # Set up TF session and initialize variables. 
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        sess = tf.Session(config=config)
        init = tf.global_variables_initializer()
        
        sess.run(init)
        
        # Load weights.
        loader = tf.train.Saver(var_list=restore_var)
        load(loader, sess, args.model_weights)
        
        # Start queue threads.
        threads = tf.train.start_queue_runners(coord=coord, sess=sess)

        start_time = time.time()
        if not os.path.exists(args.output):
          os.makedirs(args.output)
        # Perform inference.
        for step in range(num_steps):
            frame = Image.open(args.input)
            preds,  = sess.run([pred], feed_dict={image_batch:np.expand_dims(np.asarray(frame),0).astype(np.float32) })
            jpg_path=args.input
            preds[preds!=2]=0
            msk = decode_labels(preds, num_classes=args.num_classes)
            im = Image.fromarray(msk[0])
            im2=np.array(im)
            x,y,_ = np.nonzero(im2)
            img_o = Image.open(jpg_path)
            jpg_path = jpg_path.split('\\')[-1].split('.')[0]
            img = np.array(im)*0.9 + np.array(img_o)*0.7
            x ,y=np.mean(x).astype(np.int), np.mean(y).astype(np.int)
            img_o=np.array(img_o)
            img =Liquify(img_o,x,y)
            img[img>255] = 255
            img = Image.fromarray(np.uint8(img))
            img.save(args.output + '\\'+jpg_path + '.png')

if __name__ == '__main__':
    model = PGAN()
    model.run()