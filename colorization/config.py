from os.path import normpath, abspath, join, dirname, realpath


config = {'pre_trained_model_dir': normpath(abspath(join(dirname(realpath(__file__)), 'models'))),
          'meta_file_name': 'model.ckpt-3000000.meta',
          'input_height': 224,
          'input_width': 224,
          'input_channels': 1,
          'pre_trained_url': 'https://github.com/Armour/Automatic-Image-Colorization/releases/download/2.0/pre_trained.zip'}