import os
import shutil
import glob
from urllib.request import urlretrieve
from zipfile import ZipFile
import progressbar

import tensorflow as tf


class Colorizer:

    def __init__(self,
                 config):

        self.config = config
        self.models_dir = self.config['pre_trained_model_dir']
        self.meta_path = os.path.join(self.models_dir,
                                      self.config['meta_file_name'])
        self.pre_trained_url = self.config['pre_trained_url']
        self._download()
        self.imported_meta, self.session, self.input_tensor, self.pred_tensor = self._load_model()

    def _download(self):

        if not os.path.exists(self.models_dir):
            os.mkdir(self.models_dir)

        if os.path.exists(self.meta_path):
            print('pre-trained model already exists.')
            return

        zipfile_name = 'pre_trained.zip'
        zipfile_path = os.path.join(os.getcwd(), zipfile_name)

        if os.path.exists(zipfile_path):
            print('zip-file already exists.')
        else:
            url = self.pre_trained_url
            print('Downloading pre-trained model ...'.format(zipfile_name))
            urlretrieve(url, zipfile_name, reporthook=MyProgressBar())

        print('Unzipping the file {} ...'.format(zipfile_name))
        with ZipFile(zipfile_name) as zipf:
            with open(self.meta_path, 'wb') as f:
                f.write(zipf.read('summary/model.ckpt-3000000.meta'))
            with open(os.path.join(self.models_dir, 'checkpoint'), 'wb') as f:
                f.write(zipf.read('summary/checkpoint'))
            with open(os.path.join(self.models_dir, 'model.ckpt-3000000.data-00000-of-00002'), 'wb') as f:
                f.write(zipf.read('summary/model.ckpt-3000000.data-00000-of-00002'))
            with open(os.path.join(self.models_dir, 'model.ckpt-3000000.data-00001-of-00002'), 'wb') as f:
                f.write(zipf.read('summary/model.ckpt-3000000.data-00001-of-00002'))
            with open(os.path.join(self.models_dir, 'model.ckpt-3000000.index'), 'wb') as f:
                f.write(zipf.read('summary/model.ckpt-3000000.index'))

        os.remove(zipfile_path)

    def _load_model(self):
        imported_meta = tf.train.import_meta_graph(self.meta_path)
        print('graph loaded.')

        graph = tf.get_default_graph()
        input_tensor = graph.get_tensor_by_name('gray_image_one_channel:0')
        pred_tensor = graph.get_tensor_by_name('predict_rgb/clip_by_value:0')

        config = tf.ConfigProto(allow_soft_placement=True,
                                device_count={'GPU': 0},
                                log_device_placement=True,
                                gpu_options=(tf.GPUOptions(allow_growth=False)))
        sess = tf.Session(config=config)
        print('session created.')
        return imported_meta, sess, input_tensor, pred_tensor

    def colorize(self, imgs):
        """Do colorization on list of images.

        Note: imgs must be GRAY(0, 1) with each image of shape (224, 224, 1).
        """

        assert len(imgs) > 0

        for img in imgs:
            h, w, c = img.shape
            assert h == self.config['input_height']
            assert w == self.config['input_width']
            assert c == self.config['input_channels']

        with self.session.as_default():
            self.imported_meta.restore(self.session, tf.train.latest_checkpoint(self.config['pre_trained_model_dir']))
            pred = self.session.run([self.pred_tensor],
                                    feed_dict={'is_training:0': False,
                                               self.input_tensor: imgs})
        return pred


class MyProgressBar:
    def __init__(self):
        self.pbar = None

    def __call__(self, block_num, block_size, total_size):
        if not self.pbar:
            self.pbar = progressbar.ProgressBar(widgets=[progressbar.Percentage(), progressbar.Bar()],
                                                maxval=total_size)
            self.pbar.start()

        downloaded = block_num * block_size
        if downloaded < total_size:
            self.pbar.update(downloaded)
        else:
            self.pbar.finish()
