import numpy as np
import tensorflow as tf
from keras import backend as K
from keras.losses import mean_absolute_error
from keras.models import load_model

from .weightnorm import AdamWithWeightnorm


class SuperResolver:

    """A class for super-resolving RGB images using WDSR-A model.

    Source: https://github.com/krasserm/super-resolution/tree/previous
    """

    def __init__(self, config):
        self.model_path = config['model_path']
        self.model = self._load_model()

    def do_super_resolution(self, img):

        """Super-resolve the input image.

        :arg img: RGB(0, 255) image.
        :returns sr: RGB(0, 255) image.
        """

        sr = self.model.predict(np.expand_dims(img, axis=0))[0]
        return sr.astype(int)

    def _load_model(self):
        _custom_objects = {
            'tf': tf,
            'AdamWithWeightnorm': AdamWithWeightnorm,
            'mae': self._mae,
            'psnr': self._psnr
        }

        _custom_objects_backwards_compat = {
            'mae_scale_2': self._mae,
            'mae_scale_3': self._mae,
            'mae_scale_4': self._mae,
            'psnr_scale_2': self._psnr,
            'psnr_scale_3': self._psnr,
            'psnr_scale_4': self._psnr
        }

        model = load_model(self.model_path, custom_objects={**_custom_objects, **_custom_objects_backwards_compat})
        print('model loaded successfully.')
        return model

    def _mae(self, hr, sr):
        hr, sr = self._crop_hr_in_training(hr, sr)
        return mean_absolute_error(hr, sr)

    def _psnr(self, hr, sr):
        hr, sr = self._crop_hr_in_training(hr, sr)
        return tf.image.psnr(hr, sr, max_val=255)

    @staticmethod
    def _crop_hr_in_training(hr, sr):
        """
        Remove margin of size scale*2 from hr in training phase.
        The margin is computed from size difference of hr and sr
        so that no explicit scale parameter is needed. This is only
        needed for WDSR models.
        """

        margin = (tf.shape(hr)[1] - tf.shape(sr)[1]) // 2

        # crop only if margin > 0
        hr_crop = tf.cond(tf.equal(margin, 0),
                          lambda: hr,
                          lambda: hr[:, margin:-margin, margin:-margin, :])

        hr = K.in_train_phase(hr_crop, hr)
        hr.uses_learning_phase = True
        return hr, sr
