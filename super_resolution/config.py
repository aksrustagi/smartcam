from os.path import normpath, abspath, join, dirname, realpath


config = {'model_path': normpath(abspath(join(dirname(realpath(__file__)), 'models', 'wdsr-a-32-x4-psnr-29.1736.h5')))}