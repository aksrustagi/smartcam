from os.path import normpath, abspath, join, dirname, realpath


config = {'models_dir': normpath(abspath(join(dirname(realpath(__file__)), 'models')))}