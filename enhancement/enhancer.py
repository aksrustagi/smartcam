import os

import numpy as np
import tensorflow as tf


class Enhancer:

    def __init__(self, config):
        self.models_dir = config['models_dir']

    def enhance(self, img, model_name='iphone_orig'):
        """Use this method to enhance your image.

        :arg img: RGB image.
        :arg model_name: pre-trained model to use. available models: iphone_orig, blackberry_orig, sony_orig
        """

        h, w, c = img.shape
        img_size = np.prod(img.shape)
        input_data = np.reshape(img, (1, img_size)) / 255

        x_ = tf.placeholder(tf.float32, [None, img_size])
        x_image = tf.reshape(x_, [-1, h, w, 3])
        enhanced = self._resnet(x_image)

        with tf.Session() as sess:
            saver = tf.train.Saver()
            saver.restore(sess, os.path.join(self.models_dir, model_name))
            enhanced_img = sess.run(enhanced,
                                    feed_dict={x_: input_data})
        return enhanced_img[0]

    def _resnet(self, input_image):
        with tf.variable_scope("generator"):
            W1 = self._weight_variable([9, 9, 3, 64], name="W1");
            b1 = self._bias_variable([64], name="b1");
            c1 = tf.nn.relu(self._conv2d(input_image, W1) + b1)

            # residual 1

            W2 = self._weight_variable([3, 3, 64, 64], name="W2");
            b2 = self._bias_variable([64], name="b2");
            c2 = tf.nn.relu(self._instance_norm(self._conv2d(c1, W2) + b2))

            W3 = self._weight_variable([3, 3, 64, 64], name="W3");
            b3 = self._bias_variable([64], name="b3");
            c3 = tf.nn.relu(self._instance_norm(self._conv2d(c2, W3) + b3)) + c1

            # residual 2

            W4 = self._weight_variable([3, 3, 64, 64], name="W4");
            b4 = self._bias_variable([64], name="b4");
            c4 = tf.nn.relu(self._instance_norm(self._conv2d(c3, W4) + b4))

            W5 = self._weight_variable([3, 3, 64, 64], name="W5");
            b5 = self._bias_variable([64], name="b5");
            c5 = tf.nn.relu(self._instance_norm(self._conv2d(c4, W5) + b5)) + c3

            # residual 3

            W6 = self._weight_variable([3, 3, 64, 64], name="W6");
            b6 = self._bias_variable([64], name="b6");
            c6 = tf.nn.relu(self._instance_norm(self._conv2d(c5, W6) + b6))

            W7 = self._weight_variable([3, 3, 64, 64], name="W7");
            b7 = self._bias_variable([64], name="b7");
            c7 = tf.nn.relu(self._instance_norm(self._conv2d(c6, W7) + b7)) + c5

            # residual 4

            W8 = self._weight_variable([3, 3, 64, 64], name="W8");
            b8 = self._bias_variable([64], name="b8");
            c8 = tf.nn.relu(self._instance_norm(self._conv2d(c7, W8) + b8))

            W9 = self._weight_variable([3, 3, 64, 64], name="W9");
            b9 = self._bias_variable([64], name="b9");
            c9 = tf.nn.relu(self._instance_norm(self._conv2d(c8, W9) + b9)) + c7

            # Convolutional

            W10 = self._weight_variable([3, 3, 64, 64], name="W10");
            b10 = self._bias_variable([64], name="b10");
            c10 = tf.nn.relu(self._conv2d(c9, W10) + b10)

            W11 = self._weight_variable([3, 3, 64, 64], name="W11");
            b11 = self._bias_variable([64], name="b11");
            c11 = tf.nn.relu(self._conv2d(c10, W11) + b11)

            # Final

            W12 = self._weight_variable([9, 9, 64, 3], name="W12");
            b12 = self._bias_variable([3], name="b12");
            enhanced = tf.nn.tanh(self._conv2d(c11, W12) + b12) * 0.58 + 0.5

        return enhanced

    @staticmethod
    def _weight_variable(shape, name):
        initial = tf.truncated_normal(shape, stddev=0.01)
        return tf.Variable(initial, name=name)

    @staticmethod
    def _bias_variable(shape, name):
        initial = tf.constant(0.01, shape=shape)
        return tf.Variable(initial, name=name)

    @staticmethod
    def _conv2d(x, W):
        return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

    @staticmethod
    def _instance_norm(net):
        batch, rows, cols, channels = [i.value for i in net.get_shape()]
        var_shape = [channels]

        mu, sigma_sq = tf.nn.moments(net, [1, 2], keep_dims=True)
        shift = tf.Variable(tf.zeros(var_shape))
        scale = tf.Variable(tf.ones(var_shape))

        epsilon = 1e-3
        normalized = (net - mu) / (sigma_sq + epsilon) ** (.5)

        return scale * normalized + shift
